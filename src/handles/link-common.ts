import {MouseEvent} from "react";
import {NavigateFunction} from "react-router-dom";

export const redirectFabric = (to: string, navigate: NavigateFunction) => {
  return (evt: MouseEvent<HTMLButtonElement>) => {
    evt.preventDefault();

    navigate(to);
  };
}
