import * as React from "react";
import { createRoot } from "react-dom/client";
import App from "./components/App";
import {FluentProvider, webDarkTheme, webLightTheme} from "@fluentui/react-components";
import {Provider} from "react-redux";
import {store} from "../stores";

/* global document, Office, module, require */

const title = "GigaChat Add-In";

const rootElement: HTMLElement = document.getElementById("container");
const root = createRoot(rootElement);

/* Render application after Office initializes */
Office.onReady(() => {
  root.render(
    <Provider store={store}>
      <App/>
    </Provider>
  );
});

if ((module as any).hot) {
  (module as any).hot.accept("./components/App", () => {
    const NextApp = require("./components/App").default;
    root.render(NextApp);
  });
}
