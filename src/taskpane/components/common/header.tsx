import React, {ReactElement} from "react";
import {makeStyles, tokens, Title1, typographyStyles} from "@fluentui/react-components";

type Props = {
  children: ReactElement
}

const useStyle = makeStyles({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

    height: "7vh",
    paddingRight: "30px",
    paddingLeft: "30px",
    marginBottom: "20px",

    backgroundColor: tokens.colorBrandBackground2,
    color: tokens.colorBrandForeground1,

    boxShadow: tokens.shadow2,
  },
  headerTitle: {
    text: typographyStyles.title2,
  }
});

const Header = (props: Props) => {
  const style = useStyle();

  return (
    <header className={style.header}>
      <div>
        <Title1 as='h1'>GigaChat Office</Title1>
      </div>

      <div>
        {props.children}
      </div>
    </header>
  );
}

export default Header;
