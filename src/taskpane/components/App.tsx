import * as React from "react";
import {FluentProvider, webDarkTheme, webLightTheme} from "@fluentui/react-components";
import SettingsPage from "./settings/settings";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import MainPage from "./main-page/main-page";
import GuidesPage from "./guides/guides";
import {useAppSelector} from "../../hooks";
import {selectThemeSettings} from "../../stores/settings/settings-selector";
import {Theme} from "../../types/settings/setting";

// @ts-ignore
const App = () => {
  const themeSetting = useAppSelector(selectThemeSettings);
  const theme = themeSetting == Theme.Light ? webLightTheme : webDarkTheme;

  return (
    <FluentProvider theme={theme}>
    <BrowserRouter>
      <Routes>
        <Route path='/taskpane.html'>
          <Route index element={<MainPage/>}/>
          <Route path='settings' element={<SettingsPage/>}/>
          <Route path='guides' element={<GuidesPage/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
    </FluentProvider>
  );
};

export default App;
