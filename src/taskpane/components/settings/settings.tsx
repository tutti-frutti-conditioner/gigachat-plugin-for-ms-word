import * as React from "react";
import {FormEvent, useCallback, useEffect, useRef, useState} from "react";
import {Button, Field, Input, Link, makeStyles, Select, ToggleButton, Tooltip} from "@fluentui/react-components";
import {useAppDispatch, useAppSelector} from "../../../hooks";
import {Scope} from "../../../types/api/auth/auth-request";
import {useNavigate} from 'react-router-dom';
import {redirectFabric} from "../../../handles/link-common";
import {
  bundleIcon,
  ChevronCircleLeftFilled,
  ChevronCircleLeftRegular,
  SaveRegular,
  WeatherMoonRegular,
} from "@fluentui/react-icons";
import {useRootStyles} from "../../../styles/rootStyle";
import Header from "../common/header";
import {useHeaderStyle} from "../../../styles/headerStyle";
import {updateAuthSettings, updateThemeSettings} from "../../../stores/settings/settings-action";
import {selectAuthSettings, selectThemeSettings} from "../../../stores/settings/settings-selector";
import {Theme} from "../../../types/settings/setting";

function convertToScopeEnum(str: string): Scope | undefined {
  return str as Scope;
}

const useStyles = makeStyles({
  buttonContainer: {
    marginTop: "40px",
    width: "100%",

    display: "flex",
    alignItems: "center",
    flexDirection: "column"
  },
});

const ChevronCircleLeftIcon = bundleIcon(ChevronCircleLeftFilled, ChevronCircleLeftRegular);

const SettingsPage = () => {
  const rootStyles = useRootStyles();
  const headerStyle = useHeaderStyle();
  const styles = useStyles();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const authSettings = useAppSelector(selectAuthSettings);
  const darkThemeEnabled = useAppSelector(selectThemeSettings) === Theme.Dark;
  const authDataRef = useRef<HTMLInputElement | null>(null);
  const scopeRef = useRef<HTMLSelectElement | null>(null);
  const [disabledSave, setDisabledSave] = useState<boolean>(true);
  const [darkThemeEnabledSwitch, setDarkThemeEnabled] = useState<boolean>(darkThemeEnabled);

  console.log(darkThemeEnabledSwitch);

  const handleSubmit = useCallback((evt: FormEvent<HTMLFormElement>) => {
    evt.preventDefault();

    if (authDataRef.current !== null) {
      let scope = convertToScopeEnum(scopeRef.current.value);
      dispatch(updateAuthSettings({scope: scope, authData: authDataRef.current.value}));
    }

    console.log(darkThemeEnabledSwitch);
    const theme = darkThemeEnabledSwitch ? Theme.Dark : Theme.Light;

    dispatch(updateThemeSettings(theme));

    setDisabledSave(true);
  }, [dispatch, darkThemeEnabledSwitch]);

  const handleChange = useCallback((evt: FormEvent<HTMLFormElement>) => {
    evt.preventDefault();

    setDisabledSave(false);
  }, [setDisabledSave]);

  useEffect(() => {
    if (authSettings != null) {
      authDataRef.current.value = authSettings.authData;
      scopeRef.current.value = authSettings.scope;
    }
  }, [authSettings]);

  const handleRedirectToMain = useCallback(redirectFabric('/taskpane.html', navigate), [navigate]);

  return (
    <div className={rootStyles.root}>
      <Header>
        <div>
          <Tooltip content='На главную' relationship='description'>
            <Link as='button' onClick={handleRedirectToMain} className={headerStyle.iconContainer}>
              <ChevronCircleLeftIcon>На главную</ChevronCircleLeftIcon>
            </Link>
          </Tooltip>
        </div>
      </Header>

      <section className={rootStyles.contentBlock}>
        <form onSubmit={handleSubmit} className={rootStyles.contentItem} onChange={handleChange}>
          <div className={rootStyles.inputContainer}>
            <Field size='large' label='Включить темную тему'>
              <ToggleButton icon={<WeatherMoonRegular/>} onClick={() => {
                setDisabledSave(false);
                setDarkThemeEnabled(!darkThemeEnabledSwitch);
              }} defaultChecked={darkThemeEnabled}/>
            </Field>
          </div>

          <div className={rootStyles.inputContainer}>
            <Field size='large' label='Авторизационные данные'>
              <Input type="password" id='authData' ref={authDataRef}/>
            </Field>
          </div>

          <div className={rootStyles.inputContainer}>
            <Field label='Scope' size="large">
              <Select id='scope' ref={scopeRef} defaultValue={Scope.Personal}>
                <option value={Scope.Personal}>{Scope.Personal}</option>
                <option value={Scope.Corporate}>{Scope.Corporate}</option>
              </Select>
            </Field>
          </div>

          <div className={styles.buttonContainer}>
            <Button type="submit" icon={<SaveRegular/>} disabled={disabledSave} appearance='primary' size='large'>
              Сохранить настройки
            </Button>
          </div>
        </form>
      </section>
    </div>
  );
}

export default SettingsPage;
