import React, {useCallback} from "react";
import {useRootStyles} from "../../../styles/rootStyle";
import Header from "../common/header";
import {
  bundleIcon, ChevronCircleLeftFilled, ChevronCircleLeftRegular
} from "@fluentui/react-icons";
import {Link, makeStyles, Text, tokens, Tooltip, typographyStyles} from "@fluentui/react-components";
import {useHeaderStyle} from "../../../styles/headerStyle";
import {redirectFabric} from "../../../handles/link-common";
import {useNavigate} from "react-router-dom";

const useStyles = makeStyles({
  guidesContent: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
  guideHeader: typographyStyles.title2,
  guideText: typographyStyles.body2,
  guideTextBold: {
    ...typographyStyles.body2,

    fontWeight: tokens.fontWeightBold
  }
});

const ChevronCircleLeftIcon = bundleIcon(ChevronCircleLeftFilled, ChevronCircleLeftRegular);

const GuidesPage = () => {
  const styles = useStyles();
  const rootStyle = useRootStyles();
  const headerStyle = useHeaderStyle();
  const navigate = useNavigate();

  const handleRedirectToMain = useCallback(redirectFabric('/taskpane.html', navigate), [navigate]);

  return (
    <div className={rootStyle.root}>
      <Header>
        <div>
          <Tooltip content='На главную' relationship='description'>
            <Link as='button' onClick={handleRedirectToMain} className={headerStyle.iconContainer}>
              <ChevronCircleLeftIcon>На главную</ChevronCircleLeftIcon>
            </Link>
          </Tooltip>
        </div>
      </Header>

      <section className={styles.guidesContent}>
        <div>
          <Text as="h3" className={styles.guideHeader} block>Как получить данные авторизации</Text>
          <Text as="p" className={styles.guideText} block>
            Для получение авторизационных данных выполните первый шаг из официальной инструкции GigaChat
            по следующей ссылке:
            <br/>
            <br/>
            <Link href="https://developers.sber.ru/docs/ru/gigachat/individuals-quickstart">Перейти к официальной
              документации GigaChat</Link>
            <br/>
            <br/>
            Для работы плагина требуется использовать значение поля <Text as='b' className={styles.guideTextBold}
                                                                          weight="bold">Авторизационные данные</Text>
          </Text>
        </div>

        <div>
          <Text as="h3" className={styles.guideHeader} block>Здесь могла быть вторая документация</Text>
          <Text as="p" className={styles.guideText} block>
            Но мы её не написали...
          </Text>
        </div>
      </section>
    </div>
  );
}

export default GuidesPage;
