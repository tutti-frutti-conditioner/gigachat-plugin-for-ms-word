import * as React from "react";
import {useEffect, useState} from "react";
import { Button, Field, Textarea, tokens, makeStyles } from "@fluentui/react-components";
import {insertText} from "../office-document";
import {v4 as uuidv4} from "uuid";
import {response} from "express";
import axios from "axios";

const useStyles = makeStyles({
  instructions: {
    fontWeight: tokens.fontWeightSemibold,
    marginTop: "20px",
    marginBottom: "10px",
  },
  textPromptAndInsertion: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  textAreaField: {
    marginLeft: "20px",
    marginTop: "30px",
    marginBottom: "20px",
    marginRight: "20px",
    maxWidth: "50%",
  },
});

const TextInsertion: React.FC = () => {
  const handleTextInsertion = async () => {

    await insertText("text");
  };


  const styles = useStyles();

  return (
      <div className={styles.textPromptAndInsertion}>
        <Field className={styles.instructions}>Click the button to insert text.</Field>
        <Button appearance="primary" disabled={false} size="large" onClick={handleTextInsertion}>
          Insert text
        </Button>
      </div>
  );
};

export default TextInsertion;
