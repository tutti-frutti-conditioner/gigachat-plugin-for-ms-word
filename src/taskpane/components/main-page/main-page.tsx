import React, {MouseEvent, useCallback, useEffect, useRef} from "react";
import {Button, Field, Label, Link, Textarea, Tooltip} from "@fluentui/react-components";
import {useAppDispatch, useAppSelector} from "../../../hooks";
import {useNavigate} from "react-router-dom";
import {selectResponse} from "../../../stores/completions/completions-selector";
import {replaceSelectedText} from "../../office-document";
import {
  bundleIcon,
  ClipboardPasteRegular, QuestionCircleFilled,
  QuestionCircleRegular, SettingsFilled, SettingsRegular
} from "@fluentui/react-icons";
import {useRootStyles} from "../../../styles/rootStyle";
import {redirectFabric} from "../../../handles/link-common";
import Header from "../common/header";
import {useHeaderStyle} from "../../../styles/headerStyle";
import InfoFeed from "./infoFeed/info-feed";
import GigachatForm from "./forms/gigachat-form";
import {selectAuthSettings} from "../../../stores/settings/settings-selector";
import {addFeedMessage, removeFeedMessageById} from "../../../stores/feed/feed-actions";
import {FeedMessage, FeedType} from "../../../types/feed/feed-message";

const QuestionCircleIcon = bundleIcon(QuestionCircleFilled, QuestionCircleRegular);
const SettingsIcon = bundleIcon(SettingsFilled, SettingsRegular);

const MainPage = () => {
  const rootStyles = useRootStyles();
  const headerStyle = useHeaderStyle();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const completionsResponse = useAppSelector(selectResponse);
  const authSettings = useAppSelector(selectAuthSettings);
  const responseRef = useRef<HTMLTextAreaElement | null>(null);

  useEffect(() => {
    if (completionsResponse != null && completionsResponse.choices.length > 0) {
      responseRef.current.value = completionsResponse.choices[0].message.content;
    }
  }, [completionsResponse]);

  useEffect(() => {
    if (authSettings === null || authSettings.authData === "") {
      const message: FeedMessage = {
        type: FeedType.Warning,
        id: "no-auth-token",
        element: (<p>Для работы приложения перейдите в настройки и введите <b>Авторизационные данные</b></p>),
      };
      dispatch(addFeedMessage(message));
    } else {
      dispatch(removeFeedMessageById("no-auth-token"))
    }
  }, [authSettings]);

  const handleRedirectToSettings = useCallback(redirectFabric('/taskpane.html/settings', navigate), [navigate]);
  const handleRedirectToGuides = useCallback(redirectFabric('/taskpane.html/guides', navigate), [navigate]);

  const handleInsertion = useCallback(async (evt: MouseEvent<HTMLButtonElement>) => {
    evt.preventDefault();

    await replaceSelectedText(responseRef.current.value);
  }, []);

  return (
    <div className={rootStyles.root}>
      <Header>
        <div>
          <Tooltip content='Страница с помощью' relationship='description'>
            <Link as='button' onClick={handleRedirectToGuides} className={headerStyle.iconContainer}>
              <QuestionCircleIcon/>
            </Link>
          </Tooltip>
          <Tooltip content='Настройки' relationship='description'>
            <Link as='button' onClick={handleRedirectToSettings} className={headerStyle.iconContainer}>
              <SettingsIcon>К настройкам</SettingsIcon>
            </Link>
          </Tooltip>
        </div>
      </Header>

      <section className={rootStyles.contentBlock}>
        <div className={rootStyles.contentItem}>
          <InfoFeed/>
        </div>

        <div className={rootStyles.contentItem}>
          <GigachatForm/>
        </div>

        <div className={rootStyles.contentItem}>
          <div className={rootStyles.inputContainer}>
            <Field label='Ответ GigaChat' size="large">
              <Textarea ref={responseRef} resize="vertical" textarea={{className: rootStyles.textarea}} disabled/>
            </Field>
          </div>

          <div>
            <Button onClick={handleInsertion} icon={<ClipboardPasteRegular/>}>Вставить текст</Button>
          </div>
        </div>
      </section>

    </div>
  );
};

export default MainPage;
