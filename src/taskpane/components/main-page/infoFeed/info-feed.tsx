import React from "react";
import {useRootStyles} from "../../../../styles/rootStyle";
import FeedBlock from "./feed-block";
import {makeStyles} from "@fluentui/react-components";
import {FeedType} from "../../../../types/feed/feed-message";
import {useAppSelector} from "../../../../hooks";
import {selectFeedMessages} from "../../../../stores/feed/feed-selector";

const useStyles = makeStyles({
  feed: {
    width: "100%",

    display: "flex",

    alignItems: "center",
    flexDirection: "column"
  }
});

const InfoFeed = () => {
  const styles = useStyles();
  const messages = useAppSelector(selectFeedMessages);

  return (
    <div className={styles.feed}>
      {messages.map((message) => {
        return (
          <FeedBlock type={message.type} feedId={message.id}>
            {message.element}
          </FeedBlock>
        );
      })}
    </div>
  );
}

export default InfoFeed;
