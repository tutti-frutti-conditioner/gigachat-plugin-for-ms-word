import {Button, makeStyles, mergeClasses, shorthands, tokens} from "@fluentui/react-components";
import React, {ReactElement, useCallback} from "react";
import {FeedType} from "../../../../types/feed/feed-message";
import {Dismiss12Filled} from "@fluentui/react-icons";
import {useAppDispatch} from "../../../../hooks";
import {removeFeedMessageById} from "../../../../stores/feed/feed-actions";

export type Props = {
  type: FeedType;
  children: ReactElement;
  feedId: string;
}

const useStyles = makeStyles({
  block: {
    position: "relative",

    maxWidth: "200px",

    paddingLeft: "10px",
    paddingRight: "10px",
    paddingTop: "10px",
    paddingBottom: "10px",

    marginBottom: "10px",

    boxShadow: tokens.shadow8,

    ...shorthands.borderRadius(tokens.borderRadiusMedium),
    ...shorthands.borderWidth("1px"),
    ...shorthands.border("solid"),
  },
  blockInfo: {
    backgroundColor: tokens.colorPaletteGreenBackground1,

    ...shorthands.borderColor(tokens.colorPaletteGreenBorder1),
  },
  blockWarning: {
    backgroundColor: tokens.colorPaletteYellowBackground1,

    ...shorthands.borderColor(tokens.colorPaletteYellowBorder1),
  },
  blockError: {
    backgroundColor: tokens.colorPaletteRedBackground1,

    ...shorthands.borderColor(tokens.colorPaletteRedBorder1),
  },

  blockClose: {
    position: "absolute",

    top: "-5px",
    right: "-5px",

    ...shorthands.padding(0),
    ...shorthands.margin(0),
  },
  blockCloseButton: {
    ...shorthands.padding(0),
    ...shorthands.margin(0),
  }
});

function selectBlockStyles(type: FeedType) {
  const styles = useStyles();

  let selectedClass: string;

  switch (type) {
    case FeedType.Info:
      selectedClass = styles.blockInfo;
      break;

    case FeedType.Warning:
      selectedClass = styles.blockWarning;
      break;

    case FeedType.Error:
      selectedClass = styles.blockError;
      break;
  }

  return mergeClasses(selectedClass, styles.block);
}

const FeedBlock = (props: Props) => {
  const styles = useStyles();
  const dispatch = useAppDispatch();

  const blockStyles = selectBlockStyles(props.type);

  const handleClose = useCallback((evt: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    evt.preventDefault();

    dispatch(removeFeedMessageById(props.feedId))
  }, []);

  return (
    <div className={blockStyles} id={props.feedId}>
      {props.children}
      <div className={styles.blockClose}>
        <Button appearance='transparent' icon={<Dismiss12Filled/>} onClick={handleClose}>
        </Button>
      </div>
    </div>
  );
}

export default FeedBlock;
