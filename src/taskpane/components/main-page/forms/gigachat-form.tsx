import {Button, Field, Label, makeStyles, Select, Spinner, Textarea, Tooltip} from "@fluentui/react-components";
import React, {ChangeEvent, FormEvent, MouseEvent, useCallback, useRef, useState} from "react";
import {requestCompletions} from "../../../../api/api-actions";
import {useAppDispatch, useAppSelector} from "../../../../hooks";
import {useRootStyles} from "../../../../styles/rootStyle";
import {Broom20Regular, CheckmarkFilled, Copy20Regular} from "@fluentui/react-icons";
import {selectAuthSettings} from "../../../../stores/settings/settings-selector";
import {copySelectedText} from "../../../office-document";
import {Scope} from "../../../../types/api/auth/auth-request";

type LoadingState = "initial" | "loading" | "loaded";

enum ScriptState {
  custom = 0,
  restatement,
  expand,
  shorten
}

function convertToString(script: ScriptState) {
  switch (script) {
    case ScriptState.restatement:
      return 'Переформулируй текст';
    case ScriptState.expand:
      return 'Расширь текст';
    case ScriptState.shorten:
      return 'Укороти текст';
    default:
      return 'Свой вопрос';
  }
}

const useStyles = makeStyles({
  requestFormHidden: {
    display: "none",
  }
});

const GigachatForm = () => {
 const [loadingState, setLoadingState] = useState<LoadingState>('initial');
 const [inputHidden, setInputHidden] = useState<boolean>(false);
  const rootStyles = useRootStyles();
  const styles = useStyles();
  const authSettings = useAppSelector(selectAuthSettings);
  const dispatch = useAppDispatch();
  const requestRef = useRef<HTMLTextAreaElement | null>(null);
  const selectedRef = useRef<HTMLTextAreaElement | null>(null);
  const scriptRef = useRef<HTMLSelectElement | null>(null);

  const handleSubmit = useCallback(async (evt: FormEvent<HTMLFormElement>) => {
    evt.preventDefault();

    if (requestRef.current !== null && scriptRef.current !== null) {
      const scriptState = ScriptState[scriptRef.current.value];
      setLoadingState('loading');
      if (scriptState === ScriptState.custom) {
        await dispatch(
          requestCompletions({message: requestRef.current.value, context: selectedRef.current.value})
        );
      }
      else {
        await dispatch(
          requestCompletions({message: convertToString(scriptState), context: selectedRef.current.value})
        );
      }
      setLoadingState('loaded');
    }
  }, [dispatch]);

  const handleCopyText = useCallback(async (evt: MouseEvent<HTMLButtonElement>) => {
    evt.preventDefault();

    console.log(selectedRef.current.value)

    selectedRef.current.value = await copySelectedText();
  }, [selectedRef]);

  const handleClearText = useCallback(async (evt: MouseEvent<HTMLButtonElement>) => {
    evt.preventDefault();

    selectedRef.current.value = "";
  }, [selectedRef]);

  const handleFormChange = useCallback((evt: FormEvent<HTMLFormElement>) => {
    evt.preventDefault();

    setLoadingState('initial');
  }, [setLoadingState]);

  const loadingIcon =
    loadingState === "loading" ? (
      <Spinner size="tiny" />
    ) : loadingState === "loaded" ? (
      <CheckmarkFilled />
    ) : null;

  let inputContainerClasses = inputHidden ? styles.requestFormHidden : rootStyles.inputContainer;

  const handleScriptChange = useCallback(() => {
    if (ScriptState[scriptRef.current.value] === ScriptState.custom) {
      setInputHidden(false);
    } else {
      setInputHidden(true);
    }
  }, [setInputHidden, scriptRef]);

  return (
    <form onSubmit={handleSubmit} onChange={handleFormChange}>
      <div className={rootStyles.inputContainer}>
        <Field label="Сценарий" size="large">
          <Select ref={scriptRef} onChange={handleScriptChange}>
            {
              Object.keys(ScriptState)
                .filter((v) => isNaN(Number(v)))
                .map((val) => {
                return (<option value={val} id={val}>{convertToString(ScriptState[val])}</option>);
              })
            }
          </Select>
        </Field>
      </div>

      <div className={inputContainerClasses}>
        <Field label="Запрос" size="large">
          <Textarea id='user-request' placeholder='Напиши свой запрос' ref={requestRef}/>
        </Field>
      </div>

      <div className={rootStyles.inputContainer}>
        <Field label="Выделенный текст" size="large">
          <Textarea id='selected-text' placeholder='Здесь будет ваш выделенный текст' ref={selectedRef}
                    disabled textarea={{className: rootStyles.textarea}}/>
        </Field>
        <div>
          <Tooltip content="Скопировать" relationship="label">
            <Button icon={<Copy20Regular/>} onClick={handleCopyText}></Button>
          </Tooltip>
          <Tooltip content="Очистить форму" relationship="label">
            <Button icon={<Broom20Regular/>} onClick={handleClearText}></Button>
          </Tooltip>
        </div>
      </div>

      <div>
        <Button type='submit' disabled={authSettings == null || authSettings.authData == ""}
                appearance='primary' icon={loadingIcon}>
          Отправить запрос
        </Button>
      </div>
    </form>
  );
}

export default GigachatForm;
