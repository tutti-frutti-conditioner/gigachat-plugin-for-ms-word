import React from "react";
import {FeedType} from "../../../types/feed/feed-message";

const UnknownErrorContent = () => {
  return (
    <p>
      Упс... Что-то пошло не так <br/>
      Мы уже разбираемся
    </p>
  );
}

export const unknownErrorId = "unknown-error";

export const UnknownErrorMessage = () => {
  return {
    type: FeedType.Error,
    id: unknownErrorId,
    element: UnknownErrorContent(),
  };
};
