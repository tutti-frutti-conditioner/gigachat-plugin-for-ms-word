import React from "react";
import {FeedType} from "../../../types/feed/feed-message";

const AuthErrorContent = () => {
  return (
    <p>
      Ошибка авторизации при запросе. <br/>
      Убедитесь что правильно ввели <b>Авторизационные данные</b>
    </p>
  );
}

export const authErrorId = "auth-error";

export const AuthErrorMessage = () => {
  return {
    type: FeedType.Error,
    id: authErrorId,
    element: AuthErrorContent(),
  };
};
