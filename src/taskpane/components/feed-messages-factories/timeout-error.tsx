import {FeedType} from "../../../types/feed/feed-message";
import React from "react";

const TimeoutErrorContent = () => {
  return (
    <p>
      Сервер слишком долго обрабатывал запрос<br/>
      Попробуйте позже
    </p>
  );
}

export const timeoutErrorId = "unknown-error";

export const TimeoutErrorMessage = () => {
  return {
    type: FeedType.Error,
    id: timeoutErrorId,
    element: TimeoutErrorContent(),
  };
};
