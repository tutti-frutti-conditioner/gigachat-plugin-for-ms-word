import React from "react";
import {FeedType} from "../../../types/feed/feed-message";

const NetworkErrorContent = () => {
  return (
    <p>
      Нет доступа к сети <br/>
      Убедитесь что правильно ввели <b>Авторизационные данные</b>
    </p>
  );
}

export const networkErrorId = "network-error";

export const NetworkErrorMessage = () => {
  return {
    type: FeedType.Error,
    id: networkErrorId,
    element: NetworkErrorContent(),
  };
};
