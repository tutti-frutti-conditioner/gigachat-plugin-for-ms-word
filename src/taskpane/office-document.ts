/* global Word console */

export const insertText = async (text: string) => {
  // Write text to the document.
  try {
    await Word.run(async (context) => {
      let body = context.document.body;
      body.insertParagraph(text, Word.InsertLocation.end);
      await context.sync();
    });
  } catch (error) {
    console.error(error);
  }
};

export const replaceSelectedText = async (text: string) => {
  try {
    await Word.run(async (context) => {
      let t = context.document.getSelection();
      t.insertText(text, "Replace");
      await context.sync();
    });
  } catch (error) {
    console.error(error)
  }
}

export const copySelectedText = async () => {
  try {
    return await Word.run(async (context) => {
      let t = context.document.getSelection();
      t.load();
      await context.sync();
      return t.text;
    });
  } catch (error) {
    console.error(error)
    return "";
  }
}
