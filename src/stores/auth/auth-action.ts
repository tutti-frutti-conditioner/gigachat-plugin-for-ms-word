import {createAction} from "@reduxjs/toolkit";
import {AuthData} from "../../types/api/auth/auth-data";

export const updateAuthData = createAction<AuthData>('auth/update-data');

export const deleteCurrentAuth = createAction('auth/delete-data');
