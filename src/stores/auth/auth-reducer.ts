import {AuthData} from "../../types/api/auth/auth-data";
import {AuthStatus} from "../../types/api/auth/auth-status";
import {createReducer} from "@reduxjs/toolkit";
import {deleteCurrentAuth, updateAuthData} from "./auth-action";

type Store = {
  authData?: AuthData;
  authStatus: AuthStatus;
}

const initialStore : Store = {
  authData: null,
  authStatus: AuthStatus.AuthRequired
}

export const authReducer = createReducer(initialStore, (builder) => {
  builder
    .addCase(updateAuthData, (state, action) => {
      state.authStatus = AuthStatus.Authorithed;
      state.authData = action.payload;
    })
    .addCase(deleteCurrentAuth, (state, _) => {
      state.authStatus = AuthStatus.AuthRequired;
      state.authData = null;
    })
});
