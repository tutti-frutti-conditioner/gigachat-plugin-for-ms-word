import {CompletionsResponse} from "../../types/api/completions/completions-response";
import {createReducer} from "@reduxjs/toolkit";
import {updateCompletionsData} from "./completions-action";

type Store = {
  completionsResponse: CompletionsResponse | null;
}

const initialStore: Store = {
  completionsResponse: null,
}

export const completionsReducer = createReducer(initialStore, (builder) => {
  builder
    .addCase(updateCompletionsData, (state, action) => {
      state.completionsResponse = action.payload;
    })
});
