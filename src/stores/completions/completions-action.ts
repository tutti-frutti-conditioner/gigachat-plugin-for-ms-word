import {createAction} from "@reduxjs/toolkit";
import {CompletionsResponse} from "../../types/api/completions/completions-response";

export const updateCompletionsData = createAction<CompletionsResponse>('completions/update');
