import {ReducerTypes} from "../reducer";
import {RootState} from "../index";

export const selectResponse = (state: RootState) => state[ReducerTypes.Completions].completionsResponse;
