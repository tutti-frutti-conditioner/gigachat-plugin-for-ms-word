import { configureStore } from '@reduxjs/toolkit';
import {reducer} from "./reducer";
import {createAuthAPIClient, createGigaChatAPIClient} from "../api/gigachat-api";
import {AxiosInstance} from "axios";

const authApiClient = createAuthAPIClient();
const gigachatApiClient = createGigaChatAPIClient();

export type ExtraArguments = {
  authApi: AxiosInstance,
  gigaApi: AxiosInstance
}

const extraArguments: ExtraArguments = {
  authApi: authApiClient,
  gigaApi: gigachatApiClient
};

export const store = configureStore({
    reducer: reducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            thunk: {
                extraArgument: extraArguments
            }
        })
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
