import {combineReducers} from "@reduxjs/toolkit";
import {authReducer} from "./auth/auth-reducer";
import {completionsReducer} from "./completions/completions-reducer";
import {settingsReducer} from "./settings/settings-reducer";
import {feedReducer} from "./feed/feed-reducer";

export enum ReducerTypes {
  Auth = 'Auth',
  Completions = 'Completions',
  Settings = 'Settings',
  Feed = 'Feed'
}

export const reducer = combineReducers({
  [ReducerTypes.Auth]: authReducer,
  [ReducerTypes.Completions]: completionsReducer,
  [ReducerTypes.Settings]: settingsReducer,
  [ReducerTypes.Feed]: feedReducer
});
