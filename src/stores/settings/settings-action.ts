import {createAction} from "@reduxjs/toolkit";
import {AuthRequest} from "../../types/api/auth/auth-request";
import {Theme} from "../../types/settings/setting";

export const updateAuthSettings = createAction<AuthRequest>('setting/auth/update');

export const updateThemeSettings = createAction<Theme>('settings/theme/update');
