import {RootState} from "../index";
import {ReducerTypes} from "../reducer";

export const selectAuthSettings = (state: RootState) => state[ReducerTypes.Settings].Settings?.Auth;

export const selectThemeSettings = (state: RootState) => state[ReducerTypes.Settings].Settings?.Theme;
