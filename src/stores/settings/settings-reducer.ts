import {Settings, Theme} from "../../types/settings/setting";
import {createReducer} from "@reduxjs/toolkit";
import {updateAuthSettings, updateThemeSettings} from "./settings-action";

type Store = {
  Settings: Settings;
}

const initialStore: Store = {
  Settings: {
    Auth: null,
    Theme: Theme.Light
  }
}

export const settingsReducer = createReducer(initialStore, (builder) => {
  builder
    .addCase(updateAuthSettings, (state, action) => {
      state.Settings.Auth = action.payload;
    })
    .addCase(updateThemeSettings, (state, action) => {
      state.Settings.Theme = action.payload;
    })
});
