import {RootState} from "../index";
import {ReducerTypes} from "../reducer";

export const selectFeedMessages = (state: RootState) => state[ReducerTypes.Feed].messages;
