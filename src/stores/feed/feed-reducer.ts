import {FeedMessage} from "../../types/feed/feed-message";
import {createReducer} from "@reduxjs/toolkit";
import {addFeedMessage, removeFeedMessageById} from "./feed-actions";

type Store = {
  messages: FeedMessage[],
}

const initialStore: Store = {
  messages: []
}

export const feedReducer = createReducer(initialStore, (builder) => {
  builder
    .addCase(addFeedMessage, (state, action) => {
      for (let message of state.messages) {
        if (message.id == action.payload.id) {
          return;
        }
      }

      state.messages.push(action.payload);
    })
    .addCase(removeFeedMessageById, (state, action) => {
      let foundIndex = -1;

      for (let i = 0; i < state.messages.length; i++) {
        if (state.messages[i].id == action.payload) {
          foundIndex = i;
          break;
        }
      }

      if (foundIndex != -1) {
        state.messages.splice(foundIndex, 1);
      }
    });
});
