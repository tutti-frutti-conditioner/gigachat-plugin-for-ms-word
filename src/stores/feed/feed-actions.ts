import {createAction} from "@reduxjs/toolkit";
import {FeedMessage} from "../../types/feed/feed-message";

export const addFeedMessage = createAction<FeedMessage>('feed/addMessage');

export const removeFeedMessageById = createAction<string>('feed/removeMessageById');
