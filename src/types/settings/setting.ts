import {AuthRequest} from "../api/auth/auth-request";

export enum Theme {
  Light,
  Dark
}

export type Settings = {
  Auth?: AuthRequest,
  Theme: Theme
};
