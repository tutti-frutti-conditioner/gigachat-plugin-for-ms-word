export type CompletionsRequest = {
  message: string;
  context: string;
};
