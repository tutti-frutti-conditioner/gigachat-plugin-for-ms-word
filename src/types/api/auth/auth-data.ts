export type AuthData = {
    access_token: string;
    expires_at: number;
};
