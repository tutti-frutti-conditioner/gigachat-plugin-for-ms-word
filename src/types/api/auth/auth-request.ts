export type AuthRequest = {
  authData: string;
  scope: Scope;
}

export enum Scope {
  Personal = 'GIGACHAT_API_PERS',
  Corporate ='GIGACHAT_API_CORP'
}
