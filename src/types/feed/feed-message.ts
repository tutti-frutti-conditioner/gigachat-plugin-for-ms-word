import {ReactElement} from "react";

export type FeedMessage = {
  id: string,
  element: ReactElement,
  type: FeedType,
}

export enum FeedType {
  Info,
  Warning,
  Error
}
