import {createAsyncThunk} from "@reduxjs/toolkit";
import {AppDispatch, ExtraArguments, RootState} from "../stores";
import {AuthData} from "../types/api/auth/auth-data";
import {AuthRequest} from "../types/api/auth/auth-request";
import {Headers} from "./header";
import {deleteCurrentAuth, updateAuthData} from "../stores/auth/auth-action";
import { v4 as uuidv4 } from "uuid";
import {CompletionsRequest} from "../types/api/completions/completions-request";
import {CompletionsResponse} from "../types/api/completions/completions-response";
import {updateCompletionsData} from "../stores/completions/completions-action";
import {FeedMessage, FeedType} from "../types/feed/feed-message";
import React from "react";
import {addFeedMessage, removeFeedMessageById} from "../stores/feed/feed-actions";
import {unknownErrorId, UnknownErrorMessage} from "../taskpane/components/feed-messages-factories/unknown-error";
import {authErrorId, AuthErrorMessage} from "../taskpane/components/feed-messages-factories/auth-error";
import {networkErrorId, NetworkErrorMessage} from "../taskpane/components/feed-messages-factories/network-error";
import {timeoutErrorId, TimeoutErrorMessage} from "../taskpane/components/feed-messages-factories/timeout-error";

type ThunkContext = {
  dispatch: AppDispatch;
  state: RootState;
  extra: ExtraArguments;
}

const isTokenExpired = (time: number) => {
  // we assume that token expired 3 minutes before actual expired
  return time - Date.now() < 1000 * 60 * 3;
}

const formatRequest = (message: string, context: string) => {
  if (context === '') {
    return message;
  }

  return `${message}\n${context}`;
}

export const fetchAuthData = createAsyncThunk<void, AuthRequest, ThunkContext>(
  'auth/update/data',
  async (authRequest, { dispatch, extra }) => {
    try {
      const { data } = await extra.authApi.post<AuthData>('/oauth', {scope: authRequest.scope},
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            [Headers.AuthHeader]: `Basic ${authRequest.authData}`,
            [Headers.RequestID]: uuidv4()
        }});

      dispatch(removeFeedMessageById(authErrorId));
      dispatch(removeFeedMessageById(networkErrorId));
      dispatch(removeFeedMessageById(unknownErrorId));
      dispatch(removeFeedMessageById(timeoutErrorId));

      dispatch(updateAuthData(data));
    } catch (err) {
      console.error(err);

      let message: FeedMessage;

      if (err.code === 'ERR_NETWORK') {
        message = NetworkErrorMessage()
      }
      else if (err.code === 'ECONNABORTED') {
        message = TimeoutErrorMessage()
      }
      else if (err.response && (err.response.status === 401 || err.response.status === 400)) {
        message = AuthErrorMessage();
      }
      else {
        message = UnknownErrorMessage();
      }

      dispatch(addFeedMessage(message));
      dispatch(deleteCurrentAuth());
    }
  }
);

export const requestCompletions = createAsyncThunk<void, CompletionsRequest, ThunkContext>(
  'completions/update',
  async (completionsRequest, {dispatch, extra, getState}) => {
    const authSettings = getState().Settings.Settings?.Auth;

    if (authSettings === null) {
      return;
    }

    try {
      let authData = getState().Auth?.authData;

      if (authData === null || isTokenExpired(authData.expires_at)) {
        await dispatch(fetchAuthData(authSettings));
        authData = getState().Auth?.authData;

        if (authData === null) {
          return;
        }
      }

      const { data } = await extra.gigaApi.post<CompletionsResponse>('/chat/completions',
        {
          model: 'GigaChat',
          messages: [
            {
              role: 'user',
              content: formatRequest(completionsRequest.message, completionsRequest.context)
            }
          ]
        },
        {
          headers: {
            [Headers.AuthHeader]: `Bearer ${authData.access_token}`,
          }
        });

      dispatch(removeFeedMessageById(authErrorId));
      dispatch(removeFeedMessageById(networkErrorId));
      dispatch(removeFeedMessageById(unknownErrorId));
      dispatch(removeFeedMessageById(timeoutErrorId));

      dispatch(updateCompletionsData(data));

    } catch (err) {
      console.error(err);

      let message: FeedMessage;

      if (err.code === 'ERR_NETWORK') {
        message = NetworkErrorMessage()
      }
      else if (err.code === 'ECONNABORTED') {
        message = TimeoutErrorMessage()
      }
      else if (err.response && (err.response.status === 401 || err.response.status === 400)) {
        message = AuthErrorMessage();
      }
      else {
        message = UnknownErrorMessage();
      }

      dispatch(addFeedMessage(message));
    }
  }
);
