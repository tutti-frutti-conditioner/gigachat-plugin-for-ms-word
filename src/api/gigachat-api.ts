import axios, {AxiosInstance} from "axios";

const GIGACHAT_BASE_URL = 'http://localhost:8080/https://gigachat.devices.sberbank.ru/api/v1';
const AUTH_BASE_URL = 'http://localhost:8080/https://ngw.devices.sberbank.ru:9443/api/v2';
const REQUEST_TIMEOUT = 60000;

export const createGigaChatAPIClient = (): AxiosInstance => (
    axios.create({
        baseURL: GIGACHAT_BASE_URL,
        timeout: REQUEST_TIMEOUT,
    })
);

export const createAuthAPIClient = (): AxiosInstance => (
    axios.create({
        baseURL: AUTH_BASE_URL,
        timeout: REQUEST_TIMEOUT,
    })
);
