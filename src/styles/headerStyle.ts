import {makeStyles, shorthands, tokens} from "@fluentui/react-components";
import {iconFilledClassName, iconRegularClassName} from "@fluentui/react-icons";

export const useHeaderStyle = makeStyles({
  iconContainer: {
    fontSize: "36px",
    ...shorthands.margin("5px"),
    color: tokens.colorCompoundBrandForeground1,
    ":active": {
      color: tokens.colorCompoundBrandForeground1Pressed
    },

    ":hover": {
      color: tokens.colorCompoundBrandForeground1Hover,

      [`& .${iconFilledClassName}`]: {
        display: "block",
      },
      [`& .${iconRegularClassName}`]: {
        display: "none",
      },
    }
  }
})
