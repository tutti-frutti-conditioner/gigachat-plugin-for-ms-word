import {makeStyles, tokens} from "@fluentui/react-components";

export const useRootStyles = makeStyles({
  root: {
    minHeight: "100vh",
  },
  contentBlock: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",

    minHeight: "60vh",

    paddingLeft: "40px",
    paddingRight: "40px",
  },
  contentItem: {
    marginBottom: "30px",

    width: "100%",
  },
  inputContainer: {
    display: "flex",
    flexDirection: "column",

    marginBottom: "20px"
  },
  textarea: {
    color: tokens.colorNeutralForeground3,
    fontSize: tokens.fontSizeBase400
  }
});
